from flask import Flask, jsonify, request, url_for, render_template, redirect, send_from_directory
from werkzeug.utils import secure_filename, safe_join
from datetime import datetime
import os, time

app = Flask(__name__)
app.config['SECRET_KEY'] = 'chavesegurasqn'

FILES_DIRECTORY = 'files/'

app.config['UPLOAD_FOLDER'] = FILES_DIRECTORY


if not os.path.exists(FILES_DIRECTORY):
    os.makedirs(FILES_DIRECTORY)

list_of_files = []

def get_all_files():
    files = os.listdir('files/')
    for f in files:

        f_path = f'./files/{f}'

        def f_time():
            time_in_s = os.path.getctime(f_path)
            formatted_time = datetime.fromtimestamp(time_in_s).strftime("%d-%m-%Y_%I:%M:%S_%p")
            return formatted_time

        def f_size():
            size = os.path.getsize(f_path)
            formatted_size = f'{size} bytes'
            return formatted_size

        def f_extension():
            ext = f.split('.')[-1]
            return ext

        file_info = {
            'nome': f,
            'adicionado_em': f_time(),
            'tamanho': f_size(),
            'tipo': f_extension()
        }

        if file_info not in list_of_files:
            list_of_files.append(file_info)
        else:
            return list_of_files
        

@app.route('/upload', methods=['GET', 'POST'])
def form_save_file():
    

    if request.method == 'POST':
        f = request.files['imagem']

        if f.filename.split('.')[-1] not in ['jpg', 'gif', 'png']:
            txt = 'Os tipos/extensões de arquivo permitidos são jpg, gif e png'
            return render_template('index.html', msg=txt), 415

        if len(f.filename) > 0:
            f_name = secure_filename(f.filename)
            f_path = safe_join(FILES_DIRECTORY, f_name)
            f.save(f_path)

            f.seek(0, os.SEEK_END)
            size = f.tell()

            if size > 1000000:
                txt = 'O tamanho do arquivo não pode ultrapassar 1MB'
                return render_template('index.html', msg=txt), 413

            for item in list_of_files:
                if f.filename in item.values():
                    txt = 'Já existe um arquivo com o mesmo nome'
                    return render_template('index.html', msg=txt), 409
        
        txt = f'Arquivo {f.filename} enviado com sucesso'

        return render_template('index.html', sent=txt), 201

    return render_template('index.html'), 200
    

@app.route('/files', methods=['GET'])
def list_files():
    get_all_files()

    update = list_of_files

    return render_template('files.html', my_list = update), 200


@app.route('/files/<tipo>')
def list_files_by_extension(tipo):
    get_all_files()

    new_list = []

    for item in list_of_files:
        if item['tipo'] == tipo:
            new_list.append(item)

    if len(new_list) > 0:
        return render_template('files.html', my_list = new_list ), 200

    return render_template('files.html', my_list = new_list, ext = tipo), 404


# @app.route('/download/<file_name>')
# def download_file(file_name):

#     print(app.config['UPLOAD_FOLDER'])

#     return send_from_directory(app.config['UPLOAD_FOLDER'], file_name)